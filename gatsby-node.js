resolve = require('path').resolve

exports.createPages = ({graphql, actions}) => {
  return graphql(
    `
      {
        allCategoriesJson {
          edges {
            node {
              name
              items {
                name
                description
                price
                dateAdded
                id
                metadata {
                  link
                  reserved
                }
                images {
                  id
                  childImageSharp {
                    fluid(maxWidth: 300, maxHeight: 350, cropFocus: CENTER) {
                      src
                      tracedSVG
                      srcWebp
                      srcSetWebp
                      srcSet
                      sizes
                      presentationWidth
                      presentationHeight
                      originalName
                      originalImg
                      base64
                      aspectRatio
                    }
                  }
                }
              }
            }
          }
        }
      }
    `,
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }

    const categoryTemplate = resolve(`./src/templates/category.tsx`)
    const itemTemplate = resolve(`./src/templates/item.tsx`)

    // Create post pages
    const categories = result.data.allCategoriesJson.edges
    categories.forEach(({node}) => {
      actions.createPage({
        path: `/categories/${node.name}/`,
        component: categoryTemplate,
        context: {
          name: node.name,
        },
      })

      node.items.forEach(item => {
        actions.createPage({
          path: `/categories/${node.name}/${item.id}`,
          component: itemTemplate,
          context: {
            item,
          },
        })
      })
    })
  })
}
