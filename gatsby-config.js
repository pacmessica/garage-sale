'use strict'

module.exports = {
  pathPrefix: `/garage-sale`,
  siteMetadata: {
    title: 'Moving Sale',
    description: '',
    siteUrl: 'https://pacmessica.gitlab.io/garage-sale',
    author: {
      name: 'Jessica Paczuski',
    },
  },
  plugins: [
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/data/`,
      },
    },
    `gatsby-transformer-json`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-styled-components`,
    `gatsby-plugin-typescript`,
  ],
}
