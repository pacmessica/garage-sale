export function capitalize(str: string) {
  const lower = String(str).toLowerCase()
  return lower.replace(/(^| )(\w)/g, x => x.toUpperCase())
}

export function formatHtml(str: string) {
  return {__html: str.replace(/(?:\r\n|\r|\n)/g, '<br/><br/>')}
}
