const STORAGE_NAME = 'wishlist'

const storage =
  (typeof window !== 'undefined' && JSON.parse(window.localStorage.getItem(STORAGE_NAME))) || {}

interface IIdMap {
  [key: string]: boolean
}

interface IWishlistStore {
  ids: string[]
}

const wishlistStore = ({} as any) as IWishlistStore

wishlistStore.ids = storage.ids || []

const wishlist = {
  get idsMap() {
    const idMap: IIdMap = {}
    for (const id of wishlistStore.ids) {
      idMap[id] = true
    }
    return idMap
  },
  add(id: string) {
    wishlistStore.ids.push(id)
    syncStorage()
  },
  remove(id: string) {
    for (let i = 0; i < wishlistStore.ids.length; i++) {
      if (wishlistStore.ids[i] === id) {
        wishlistStore.ids.splice(i, 1)
      }
    }
    syncStorage()
  },
}

function syncStorage() {
  if (typeof window !== 'undefined') {
    window.localStorage.setItem(STORAGE_NAME, JSON.stringify(wishlistStore))
  }
}

export default wishlist
