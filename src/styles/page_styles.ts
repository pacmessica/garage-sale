import {Link} from 'gatsby'
import {styled} from './theme'

export const StyledLink = styled(Link)`
  margin: 15px 0;
  display: block;
  float: none;
  background: #2d2926;
  padding: 10px 0;
  color: #fff !important;
  text-transform: uppercase;
  text-align: center;
  letter-spacing: 0.12em;
  text-decoration: none;
  border-radius: 5px;

  :hover {
    opacity: 0.8;
  }
`

export const CategoryContainer = styled.div`
  display: inline-block;
  margin: 5px;
  min-width: 80%;
  width: 100%;
  text-align: center;
  padding: 10px;
  text-decoration: none;

  @media only screen and (min-width: 760px) {
    min-width: 31%;
    max-width: 450px;
    box-shadow: 0 0 12px #aaa;
    border-radius: 2%;
    :hover {
      box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.8);
    }
  }
`

export const Flex = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  flex-wrap: wrap;

  .flex-item {
    flex: 1;
  }
`

export const Seperator = styled.hr`
  width: 100%;
  color: #2d2926;
`

export const ItemsContainer = styled.div`
  display: flex;
  align-content: stretch;
  flex-flow: wrap;
  justify-content: space-around;
`

export const Item = styled.div`
  width: 50%;
  box-sizing: border-box;
  padding: 5px;
`
