import {Link} from 'gatsby'

import {styled} from './theme'

export const Flex = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
`

export const Item = styled(Link)`
  width: 100%;
  position: relative;
  padding: 5px;
  box-sizing: border-box;
  text-align: center;
  text-decoration: none;
  display: flex;
  margin: 0 0 10px 0;
  box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);
  border-radius: 8px;

  @media only screen and (min-width: 760px) {
    flex-direction: column;
    width: 22%;
    min-width: 200px;
    min-height: 350px;
    box-sizing: border-box;
    margin: 5px;
    border-radius: 2%;
    box-shadow: 0 0 12px #aaa;

    :hover {
      box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.8);
    }
  }
`

export const ProductLabel = styled.div`
  width: 0px;
  height: 0px;
  border-bottom: 90px solid #415d4c;
  border-left: 90px solid transparent;
  bottom: 0;
  right: 0;
  position: absolute;
  z-index: 10;
  text-transform: uppercase;
  .label {
    color: #ffffff;
    width: 110px;
    position: absolute;
    padding: 1%;
    top: 44px;
    right: -25px;
    transform: rotate(-46deg);
  }
`

export const ImageContainer = styled.div`
  flex: 1;
`

export const Info = styled.div`
  flex: 3;
  padding: 5px;

  .title {
    font-size: 1.2rem;
    margin: 0.5rem;
  }
  .price {
    display: block;
    font-size: 1.5rem;
    color: #caaf8c;
    text-align: center;
    font-weight: bold;
  }

  @media only screen and (min-width: 760px) {
    width: 100%;
  }
`
