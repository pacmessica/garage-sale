import {styled} from './theme'

export const Flex = styled.div`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  align-items: flex-start;
  flex-direction: column;

  @media only screen and (min-width: 760px) {
    flex-direction: row;
  }
`

export const FlexItem = styled.div<{flex: number}>`
  flex: ${props => props.flex};
  min-width: 100%;

  @media only screen and (min-width: 760px) {
    padding-right: 2%;
    min-width: auto;
  }
  img {
    margin: 5px;
  }
`

export const Seperator = styled.hr`
  width: 100%;
  color: #2d2926;
`
