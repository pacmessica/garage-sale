import {styled} from './theme'

export const Container = styled.section`
  display: flex;
  flex-wrap: wrap;

  .dots {
    width: 100%;
    padding: 0;
    margin: 0;
    list-style: none;
    text-align: center;
  }

  .dot {
    background: #2d2926;
    display: inline-block;
    border-radius: 50%;
    width: 15px;
    height: 15px;
    margin: 0 15px;
    padding: 0;
    opacity: 0.3;
  }

  .dot-active {
    opacity: 0.8;
  }
`

export const Info = styled.div`
  flex: none;
  width: 90%;
  padding: 5%;

  .title {
    display: inline-block;
    font-size: 25px;
    font-weight: 700;
    margin: 0;
  }
  .price {
    display: inline-block;
    font-size: 25px;
    color: #caaf8c;
    font-weight: 700;
    padding: 10px;
    text-align: right;
    margin-left: 2%;
  }
  .date {
    display: inline-block;
    font-size: 16;
    color: #999;
    text-align: left;
  }

  a {
    cursor: pointer;
  }

  @media only screen and (min-width: 760px) {
    flex: 1;
    width: 50%;
    .title {
      width: 80%;
    }
  }
`
export const ImageContainer = styled.div`
  width: 100%;

  @media only screen and (min-width: 760px) {
    max-width: 450px;
    .gatsby-image-wrapper {
      margin-bottom: 10px;
    }
  }
`

export const AddToWishList = styled.div`
  padding-top: 20px;

  button {
    background: #415d4c;
    color: white;
    font-weight: lighter;
    width: 100%;
    padding: 10px;
    font-size: 1rem;
    cursor: pointer;
  }

  .saved {
    color: #415d4c;
    font-size: 1.3rem;
  }

  @media only screen and (min-width: 760px) {
    button {
      width: 200px;
    }
  }
`
