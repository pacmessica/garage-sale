import {Link} from 'gatsby'
import React from 'react'
import {GlobalStyle, styled} from '../styles/theme'
import wishlist from '../wishlistStore'

const StyledNav = styled.nav`
  color: white;
  background-color: #2d2926;
  padding-left: 20px;

  ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
  }

  li {
    display: inline-block;
    margin: 16px;

    a {
      text-decoration: none;
      background: none;
    }
  }
`

interface IProps {
  readonly title?: string
  readonly wishlistItemCount?: number
}

export default class Layout extends React.Component<IProps> {
  public render() {
    const {children} = this.props
    let {wishlistItemCount} = this.props
    if (wishlistItemCount === undefined) {
      wishlistItemCount = Object.keys(wishlist.idsMap).length
    }

    return (
      <>
        <GlobalStyle />
        <StyledNav className="navigation">
          <ul>
            <li>
              <Link to={`/`}>All Categories</Link>
            </li>
            <li>
              <Link to={`/about`}>About</Link>
            </li>
            <li>
              <Link to={`/wishlist`}>
                Wishlist {!!wishlistItemCount ? `(${wishlistItemCount})` : ''}
              </Link>
            </li>
          </ul>
        </StyledNav>
        <main className="content" role="main">
          {children}
        </main>
      </>
    )
  }
}
