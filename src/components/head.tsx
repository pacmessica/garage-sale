import {graphql, StaticQuery} from 'gatsby'
import React from 'react'
import Helmet from 'react-helmet'

interface IStaticQueryData {
  site: {
    siteMetadata: {
      title: string
      description: string
      author: {
        name: string
      }
    }
  }
}

interface IProps {
  readonly title: string
  readonly description?: string
  readonly lang?: string
  readonly keywords?: string[]
}

export default class Head extends React.Component<IProps> {
  public render() {
    return (
      <StaticQuery
        query={graphql`
          query {
            site {
              siteMetadata {
                title
                description
                author {
                  name
                }
              }
            }
          }
        `}
        render={(data: IStaticQueryData) => {
          const metaDescription = this.props.description || data.site.siteMetadata.description
          const lang = this.props.lang || 'en'
          const title = this.props.title
          const keywords = this.props.keywords || []
          return (
            <Helmet
              htmlAttributes={{
                lang,
              }}
              title={title}
              titleTemplate={`%s | ${data.site.siteMetadata.title}`}
              meta={[
                {
                  content: metaDescription,
                  name: `description`,
                },
                {
                  content: title,
                  property: `og:title`,
                },
                {
                  content: metaDescription,
                  property: `og:description`,
                },
                {
                  content: `website`,
                  property: `og:type`,
                },
                {
                  content: `summary`,
                  name: `twitter:card`,
                },
                {
                  content: data.site.siteMetadata.author.name,
                  name: `twitter:creator`,
                },
                {
                  content: title,
                  name: `twitter:title`,
                },
                {
                  content: metaDescription,
                  name: `twitter:description`,
                },
              ].concat(
                keywords.length > 0
                  ? {
                      content: keywords.join(`, `),
                      name: `keywords`,
                    }
                  : [],
              )}
            />
          )
        }}
      />
    )
  }
}
