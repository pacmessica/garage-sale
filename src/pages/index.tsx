import {graphql} from 'gatsby'
import Img, {FluidObject} from 'gatsby-image'
import React from 'react'
import Head from '../components/head'
import Layout from '../components/layout'

import {
  CategoryContainer,
  Flex,
  Item,
  ItemsContainer,
  Seperator,
  StyledLink,
} from '../styles/page_styles'

interface IProps {
  readonly data: IPageQueryData
}

interface IState {
  isMobile: boolean
}

export default class Index extends React.Component<IProps, IState> {
  public state = {
    isMobile: false,
  }

  public componentDidMount() {
    if (typeof navigator !== 'undefined') {
      this.setState({
        isMobile: /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
          navigator.userAgent,
        ),
      })
    }
  }

  public render() {
    const {data} = this.props
    const siteTitle = 'categories'
    const categories = data.allCategoriesJson.edges
    const maxItems = this.state.isMobile ? 2 : 4

    return (
      <Layout title={siteTitle}>
        <Head title="All categories" keywords={[`blog`, `gatsby`, `javascript`, `react`]} />
        <Flex>
          {categories
            .sort((a, b) =>
              a.node.weight === b.node.weight ? 0 : +(a.node.weight < b.node.weight) || -1,
            )
            .map(({node}) => {
              return (
                <CategoryContainer key={node.name} className="flex-item">
                  <h3>{node.displayName || node.name}</h3>
                  <Seperator />
                  <ItemsContainer>{this.formatItems(node.items.slice(0, maxItems))}</ItemsContainer>
                  <StyledLink to={`/categories/${node.name}`}>
                    See All {node.displayName || node.name}
                  </StyledLink>
                </CategoryContainer>
              )
            })}
        </Flex>
      </Layout>
    )
  }

  private formatItems = (items: IItemData[]) => {
    return items.map((item: IItemData) => {
      if (!item.images[0]) {
        return
      }

      const {id, childImageSharp} = item.images[0]

      return (
        <Item key={id}>
          <Img fluid={childImageSharp.fluid} />
        </Item>
      )
    })
  }
}

interface IPageQueryData {
  allCategoriesJson: {
    edges: Array<{node: {name: string; displayName: string; weight: number; items: IItemData[]}}>
  }
}

interface IItemData {
  images: Array<{id: number; childImageSharp: {fluid: FluidObject}}>
}

export const pageQuery = graphql`
  query {
    allCategoriesJson {
      edges {
        node {
          name
          displayName
          weight
          items {
            name
            images {
              id
              childImageSharp {
                fluid(maxWidth: 215, maxHeight: 145) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
