import {graphql} from 'gatsby'
import React from 'react'

import Head from '../components/head'
import Layout from '../components/layout'

interface IProps {
  readonly data: IPageQueryData
}

export default class NotFoundPage extends React.Component<IProps> {
  public render() {
    const {data} = this.props
    const siteTitle = data.site.siteMetadata.title

    return (
      <Layout title={siteTitle}>
        <Head title="404: Not Found" />
        <h1>Not Found</h1>
        <p>This listing could not be found. It has likely been sold.</p>
      </Layout>
    )
  }
}

interface IPageQueryData {
  site: {
    siteMetadata: {
      title: string
    }
  }
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`
