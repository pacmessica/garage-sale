import {graphql} from 'gatsby'
import Img, {FluidObject} from 'gatsby-image'
import React from 'react'
import Head from '../components/head'
import Layout from '../components/layout'
import {Flex, FlexItem} from '../styles/about_styles'

interface IProps {
  readonly data: IPageQueryData
}

export default class About extends React.Component<IProps> {
  public render() {
    const {data} = this.props
    const siteTitle = data.site.siteMetadata.title
    const json = data.allAboutJson.edges[0]
    return (
      <Layout title={siteTitle}>
        <Head title="About" keywords={[`blog`, `gatsby`, `javascript`, `react`]} />
        <section>
          <h1>The Sale</h1>
          <Flex>
            <FlexItem flex={2}>
              <p>
                After almost 4 years of calling Amsterdam home, we are leaving the city (and most of
                our stuff!) behind us as we head off to new adventures in Canada at the end of
                October.
                <br />
                <br />
                We purchased the majority of our furniture new from ikea. For the items that are
                still in excellent condition we are asking for 40-50% of the original price. Other
                more "used" items are marked down further.
                <br />
                <br />
                Priority for all cheaper/free items will be given to people purchasing furniture &
                electronics. We will try to update the site as often as possible to best represent
                the availability of items.
                <br />
                <br />
                <strong>
                  Pick up for all items is in the center of Amsterdam (across from Rokin metro
                  station) after October 14th. Pick up is possible every morning before noon (Monday
                  - Sunday). If you can absolutely not make it between those times, please let us
                  know and we can figure out an alternative.
                </strong>
              </p>
            </FlexItem>
            <FlexItem flex={1}>
              {!!json &&
                json.node.images.map(({childImageSharp, id}) => {
                  return <Img key={id} fluid={childImageSharp.fluid} />
                })}
            </FlexItem>
          </Flex>
        </section>
      </Layout>
    )
  }
}

interface IPageQueryData {
  site: {
    siteMetadata: {
      title: string
    }
  }
  allAboutJson: {
    edges: Array<{
      node: {
        images: Array<{
          id: number
          childImageSharp: {fluid: FluidObject}
        }>
      }
    }>
  }
}

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allAboutJson {
      edges {
        node {
          images {
            id
            childImageSharp {
              fluid(maxWidth: 350, maxHeight: 250) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
`
