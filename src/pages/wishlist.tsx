import {graphql, Link} from 'gatsby'
import Img, {FluidObject} from 'gatsby-image'
import React from 'react'
import styled from 'styled-components'
import Head from '../components/head'
import Layout from '../components/layout'
import {capitalize} from '../utils/strings'
import wishlist from '../wishlistStore'

const Table = styled.table`
  color: hsl(0, 0%, 40%);
  border: 1px solid #dfe4e6;
  width: 100%;
  border-spacing: 0;
  border-collapse: collapse;

  thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
  }

  th {
    text-transform: uppercase;
    font-weight: 700;
  }
  th,
  td {
    border-right: 1px solid #e7e7e7;
    border-bottom: 1px solid #dfe4e6;
    padding: 20px;
    text-align: left;
  }

  .image-col {
    width: 150px;
  }

  .remove-col {
    width: 20px;
    text-align: center;
  }

  .remove {
    cursor: pointer;
  }

  .total {
    font-weight: bold;
    font-size: large;
  }
`

const WishlistContainer = styled.section`
  max-width: 1000px;
  margin: auto;
`

interface IProps {
  readonly data: IPageQueryData
}

interface IState {
  items: IWishlistItemData[]
}

interface IWishlistItemData {
  id: string
  name: string
  categoryName: string
  price: number
  images: Array<{id: number; childImageSharp: {fluid: FluidObject}}>
}

export default class Wishlist extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {
      items: [],
    }
  }

  public componentDidMount() {
    const items = this.getWishListItems()
    this.setState({items})
  }

  public getWishListItems = () => {
    const categories = this.props.data.allCategoriesJson.edges
    const wishlistitems: IWishlistItemData[] = []
    const lookup = wishlist.idsMap

    categories.forEach(({node}) => {
      node.items.forEach(item => {
        if (!!lookup[item.id]) {
          const wlItem = item as IWishlistItemData
          wlItem.categoryName = node.name
          wishlistitems.push(wlItem)
        }
      })
    })

    return wishlistitems
  }

  public handleRemoveItem = (id: string) => {
    const items = [...this.state.items]
    for (let i = 0; i < items.length; i++) {
      if (items[i].id === id) {
        items.splice(i, 1)
      }
    }
    this.setState({items})
    wishlist.remove(id)
  }

  public generateMailTo = () => {
    const {items} = this.state
    const totalPrice = items.reduce((sum, item) => sum + item.price, 0)
    const subject = `Request: ${totalPrice} euro for ${items.length} items`
    const body = `Hello, \n\nI'd like to make a request for the following items:\n\n ${items
      .map(item => `- ${capitalize(item.name)}`)
      .join(' \n')}`
    return `mailto:amsterdam.october.moving.sale@gmail.com?subject=${subject}&body=${encodeURIComponent(
      body,
    )}`
  }

  public render() {
    const {items} = this.state
    return (
      <Layout title={'wishlist'} wishlistItemCount={items.length}>
        <Head title="WishList" keywords={[`amsterdam`, `gatsby`, `javascript`, `react`]} />
        <WishlistContainer>
          <h1>WishList</h1>
          {!items.length ? (
            <div>Wishlist is empty!</div>
          ) : (
            <>
              <p>
                Send us an email with your request:{' '}
                <strong>
                  <a href={this.generateMailTo()}>Email us here!</a>
                </strong>
              </p>
              <p>
                Please note that this is a request only and not all items may be available. Priority
                for cheaper items will be given to people also buying furniture.
              </p>
              <p>
                Pick up for all items is in the center of Amsterdam (across from Rokin metro
                station) <strong>after October 14th</strong>. Pick up is possible every morning
                before noon (Monday - Sunday). If you can absolutely not make it between those
                times, please let us know and we can figure out an alternative.
              </p>
              <br></br>
              <br></br>
              <Table>
                <thead>
                  <tr>
                    <th className="image-col"></th>
                    <th className="name-col">Item</th>
                    <th className="price-col">Price</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {items.map(({id, name, categoryName, price, images}) => {
                    return (
                      <tr key={id}>
                        <td className="image-col">
                          {!!images[0] && (
                            <Link to={`/categories/${categoryName}/${id}`}>
                              <Img fluid={images[0].childImageSharp.fluid} />
                            </Link>
                          )}
                        </td>
                        <td className="name-col">{capitalize(name)}</td>
                        <td className="price-col">&euro; {price}</td>
                        <td className="remove-col">
                          <div className="remove" onClick={() => this.handleRemoveItem(id)}>
                            x
                          </div>
                        </td>
                      </tr>
                    )
                  })}
                  <tr className="total">
                    <td colSpan={2}>Total Price</td>
                    <td>&euro; {items.reduce((sum, item) => sum + item.price, 0)}</td>
                    <td></td>
                  </tr>
                </tbody>
              </Table>
            </>
          )}
        </WishlistContainer>
      </Layout>
    )
  }
}

interface IPageQueryData {
  allCategoriesJson: {
    edges: Array<{node: {name: string; displayName: string; items: IItemData[]}}>
  }
}

interface IItemData {
  id: string
  name: string
  price: number
  images: Array<{id: number; childImageSharp: {fluid: FluidObject}}>
}

export const pageQuery = graphql`
  query {
    allCategoriesJson {
      edges {
        node {
          name
          items {
            id
            name
            price
            images {
              id
              childImageSharp {
                fluid(maxWidth: 300, maxHeight: 200) {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`
