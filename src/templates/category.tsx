import {graphql} from 'gatsby'
import Img, {FluidObject} from 'gatsby-image'
import React from 'react'
import Head from '../components/head'
import Layout from '../components/layout'
import {Flex, ImageContainer, Info, Item, ProductLabel} from '../styles/category_styles'
import {capitalize} from '../utils/strings'

interface IProps {
  readonly data: IPageQueryData
}

export default class CategoryTemplate extends React.Component<IProps> {
  public render() {
    if (!(this.props.data && this.props.data.allFile && this.props.data.allFile.edges)) {
      return <div>not found</div>
    }

    const category = this.props.data.allFile.edges[0].node.childCategoriesJson
    return (
      <Layout title={category.name}>
        <Head title={`All ${capitalize(category.name)}`} description="some description" />
        <section>
          <header>
            <h1>All {category.displayName || category.name}</h1>
          </header>
          <Flex>
            {category.items
              .sort((a, b) => {
                if (a.metadata.reserved && !b.metadata.reserved) {
                  return 1 // b before a
                }
                if (!a.metadata.reserved && b.metadata.reserved) {
                  return -1 // a before b
                }
                return 0
              })
              .map(({price, name, images, id, metadata}) => {
                return (
                  <Item key={name} to={`/categories/${category.name}/${id}`}>
                    {metadata.reserved && (
                      <ProductLabel>
                        <span className="label">reserved</span>
                      </ProductLabel>
                    )}
                    {images.length > 0 && images[0] !== null && (
                      <ImageContainer>
                        <Img fluid={images[0].childImageSharp.fluid} />
                      </ImageContainer>
                    )}
                    <Info>
                      <h1 className="title">{capitalize(name)}</h1>
                      <div className="price">&euro; {price}</div>
                    </Info>
                  </Item>
                )
              })}
          </Flex>
        </section>
      </Layout>
    )
  }
}

interface IItemData {
  name: string
  id: string
  description: string
  price: number
  dateAdded: string
  images: Array<{childImageSharp: {fluid: FluidObject}}>
  metadata: {reserved: boolean}
}

interface IPageQueryData {
  allFile: {
    edges: Array<{
      node: {childCategoriesJson: {name: string; displayName: string; items: IItemData[]}}
    }>
  }
}

export const pageQuery = graphql`
  query CategoryByName($name: String!) {
    allFile(filter: {name: {eq: $name}}) {
      edges {
        node {
          childCategoriesJson {
            name
            displayName
            items {
              name
              id
              description
              price
              dateAdded
              metadata {
                reserved
              }
              images {
                childImageSharp {
                  fluid(maxWidth: 300, maxHeight: 350, cropFocus: CENTER) {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`
