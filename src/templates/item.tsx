import Img, {FluidObject} from 'gatsby-image'
import React from 'react'
import {Swipeable} from 'react-swipeable'
import Head from '../components/head'
import Layout from '../components/layout'
import {AddToWishList, Container, ImageContainer, Info} from '../styles/item_styles'
import {capitalize, formatHtml} from '../utils/strings'
import wishlist from '../wishlistStore'

interface IProps {
  readonly pageContext: IPageQueryData
}

interface IState {
  imageIndex: number
  isMobile: boolean
  wishlisted: boolean
  wishlistItemCount: number
}

export default class ItemTemplate extends React.Component<IProps, IState> {
  public state = {
    imageIndex: 0,
    isMobile: false,
    wishlistItemCount: 0,
    wishlisted: false,
  }

  public componentDidMount() {
    if (typeof navigator !== 'undefined') {
      this.setState({
        isMobile: /Android|webOS|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
          navigator.userAgent,
        ),
      })
    }
    const wishlisted = this.isWishListed()
    const wishlistItemCount = Object.keys(wishlist.idsMap).length
    this.setState({wishlisted, wishlistItemCount})
  }

  public isWishListed() {
    const {id} = this.props.pageContext.item
    return wishlist.idsMap[id]
  }

  public addToWishlist(id: string) {
    wishlist.add(id)
    this.setState({wishlistItemCount: this.state.wishlistItemCount + 1, wishlisted: true})
  }

  public render() {
    const {description, images, name, dateAdded, price, metadata, id} = this.props.pageContext.item
    const {imageIndex, wishlistItemCount, wishlisted} = this.state
    return (
      <Layout title="hello" wishlistItemCount={wishlistItemCount}>
        <Head title={name} description="some description" />
        <Container>
          {images.length > 1 && this.state.isMobile ? (
            <ImageContainer>
              <Swipeable
                onSwipedLeft={() => this.onSwiped({left: true})}
                onSwipedRight={() => this.onSwiped({right: true})}>
                <Img key={images[imageIndex].id} fluid={images[imageIndex].childImageSharp.fluid} />
              </Swipeable>
              <ul className="dots">
                {images.map(({}, index) => {
                  return (
                    <li
                      key={index}
                      className={`dot ${index === imageIndex ? 'dot-active' : ''}`}></li>
                  )
                })}
              </ul>
            </ImageContainer>
          ) : (
            <ImageContainer>
              {images.map(image => {
                return <Img key={image.id} fluid={image.childImageSharp.fluid} />
              })}
            </ImageContainer>
          )}
          <Info>
            <h1 className="title">
              {metadata.reserved ? '[RESERVED]' : ''} {capitalize(name)}{' '}
            </h1>
            <div className="price">&euro; {price}</div>
            <div className="date">Added on {dateAdded}</div>
            <p className="description" dangerouslySetInnerHTML={formatHtml(description)} />
            {!!metadata.link && (
              <div>
                <a href={metadata.link} target="_blank">
                  See more
                </a>
              </div>
            )}
            <br></br>
            {!!metadata.reserved && (
              <div>
                Please note this item has been reserved by another person but they have not yet
                picked it up. You can still request this item and if it doesn't work out with the
                other person, we'll let you know!
              </div>
            )}
            <AddToWishList>
              {!wishlisted ? (
                <button onClick={() => this.addToWishlist(id)}>Add to Wishlist</button>
              ) : (
                <div className="saved">saved to wishlist!</div>
              )}
            </AddToWishList>
          </Info>
        </Container>
      </Layout>
    )
  }

  private onSwiped = ({right}: {left?: boolean; right?: boolean}) => {
    const {imageIndex} = this.state
    const {images} = this.props.pageContext.item
    const maxIndex = images.length - 1

    let newIndex = imageIndex !== maxIndex ? imageIndex + 1 : 0
    if (!!right) {
      newIndex = imageIndex !== 0 ? imageIndex - 1 : maxIndex
    }
    this.setState({imageIndex: newIndex})
  }
}

interface IPageQueryData {
  item: {
    price: number
    id: string
    name: string
    description: string
    dateAdded: string
    images: Array<{id: number; childImageSharp: {fluid: FluidObject}}>
    metadata: {link: string; reserved: boolean}
  }
}
